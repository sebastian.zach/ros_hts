# Ros node for HTS Audio Transformer Classification

## Build

``` sh
git submodule --init --recursive

docker build -t hts_ros --ssh default .
```

## Run

Download hts model (see their docs)

``` sh
pactl load-module module-native-protocol-tcp  port=34567
docker run -it --net=host --gpus all \
    --privileged \
    --env="NVIDIA_DRIVER_CAPABILITIES=all" \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="PULSE_SERVER=0.0.0.0:34567"
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    -v "$SSH_AUTH_SOCK:$SSH_AUTH_SOCK" -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK \
    -v /path/to/model.ckpt:/model.ckpt \
hts_ros:latest
```

