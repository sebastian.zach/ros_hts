#!/usr/bin/env python3
import csv
import os
import warnings

import hts.config as config
import numpy as np
import rospy
import torch
import torch.distributed as dist
from hts.model.htsat import HTSAT_Swin_Transformer
from hts.sed_model import SEDWrapper
from ros_hts.msg import AudioClass

import soundfile as sf
from pysoundio import (
    PySoundIo,
    SoundIoFormatFloat32LE,
)

warnings.filterwarnings("ignore")


class Record:
    def __init__(
        self,
        backend=None,
        input_device=None,
        sample_rate=None,
        block_size=None,
        channels=None,
        outfile=None,
    ):
        self.channels = channels
        self.pysoundio = PySoundIo(backend=backend)
        self.input_device = input_device
        self.sample_rate = sample_rate
        self.block_size = block_size

        self.wav_file = None
        if outfile is not None:
            self.wav_file = sf.SoundFile(
                outfile, mode="w", channels=channels, samplerate=sample_rate
            )
        self.numpy_data = np.empty((0, channels))

    def start(self):
        self.pysoundio.start_input_stream(
            device_id=self.input_device,
            channels=self.channels,
            sample_rate=self.sample_rate,
            block_size=self.block_size,
            dtype=SoundIoFormatFloat32LE,
            read_callback=self.callback,
        )

    def close(self):
        self.pysoundio.close()
        if self.wav_file is not None:
            self.wav_file.close()

    def clear(self):
        self.numpy_data = np.empty((0, self.channels))

    def callback(self, data, length):
        # convert bytearray to numpy array
        np_data = np.frombuffer(data, dtype=np.float32).reshape((-1, self.channels))
        self.numpy_data = np.concatenate((self.numpy_data, np_data), axis=0)

        if self.wav_file is not None:
            self.wav_file.buffer_write(data, dtype="float32")

    def list_devices(self):
        input_devices, _ = self.pysoundio.list_devices()
        for i, device in enumerate(input_devices):
            msg = "* " if device["is_default"] else "  "
            msg += str(i) + " - " + device["name"]
            print(msg)
            print("\tsample rate:")
            print("\t default: {}Hz".format(device["sample_rates"]["current"]))
            print(
                "\t available: {}".format(
                    ", ".join(
                        [
                            str(d["max"]) + "Hz"
                            for d in device["sample_rates"]["available"]
                        ]
                    )
                )
            )
            print(
                "\tformat: {}".format(
                    ", ".join([str(d) for d in device["formats"]["available"]])
                )
            )
            print("\tlayouts: {}".format(device["layouts"]["current"]["name"]))
            print(
                "\t available: {}".format(
                    ", ".join([str(d["name"]) for d in device["layouts"]["available"]])
                )
            )
            print("\tsoftware latency:")
            print(
                "\t min: {}s, max: {}s, current: {}s".format(
                    round(device["software_latency_min"], 4),
                    round(device["software_latency_max"], 4),
                    round(device["software_latency_current"], 4),
                )
            )
            print("")


class HTSNode:
    def __init__(self):
        self.audio_backend = 2  # Pulse
        self.input_device = 3
        self.channels = 2

        classification_topic = "audio_classification"

        self.pub = rospy.Publisher(classification_topic, AudioClass, queue_size=10)

        rospy.init_node("hts_node", anonymous=True)

        config.resume_checkpoint = "/model.ckpt"
        csv_path = "/catkin_ws/src/hts/src/hts/class_label_indice.csv"

        os.environ["MASTER_ADDR"] = "localhost"
        os.environ["MASTER_PORT"] = "12355"
        dist.init_process_group(backend="nccl", rank=0, world_size=1)

        sed_model = HTSAT_Swin_Transformer(
            spec_size=config.htsat_spec_size,
            patch_size=config.htsat_patch_size,
            in_chans=1,
            num_classes=config.classes_num,
            window_size=config.htsat_window_size,
            config=config,
            depths=config.htsat_depth,
            embed_dim=config.htsat_dim,
            patch_stride=config.htsat_stride,
            num_heads=config.htsat_num_head,
        )

        self.model = SEDWrapper(
            sed_model=sed_model,
            config=config,
            dataset=None,
        )

        ckpt = torch.load(config.resume_checkpoint, map_location="cpu")
        ckpt["state_dict"].pop("sed_model.head.weight")
        ckpt["state_dict"].pop("sed_model.head.bias")
        self.model.load_state_dict(ckpt["state_dict"], strict=False)

        # load class labels csv as array
        self.display_names = []
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.display_names.append(row["display_name"])

        self.record = Record(
            backend=self.audio_backend,
            input_device=self.input_device,
            sample_rate=config.sample_rate,
            channels=self.channels,
        )

        # self.record.list_devices()

    def run(self):
        rate = rospy.Rate(1 / 4)
        self.record.start()
        while not rospy.is_shutdown():
            rate.sleep()
            x = self.record.numpy_data.T

            output_dic = self.model.inference(x)
            categories = output_dic["clipwise_output"]
            most_confident = np.argmax(categories, axis=1)

            audioClassMsg = AudioClass()
            audioClassMsg.classifications = []
            for i in range(categories.shape[0]):
                audioClassMsg.classifications.append(
                    self.display_names[most_confident[i]]
                )

            self.pub.publish(audioClassMsg)
            self.record.clear()


if __name__ == "__main__":
    try:
        htsNode = HTSNode()
        htsNode.run()
    except rospy.ROSInterruptException:
        print("ROSInterruptException")
        htsNode.close()
        pass
