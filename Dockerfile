FROM condaforge/mambaforge

SHELL ["/bin/bash", "-c"]
# RUN conda init bash
RUN mamba init bash

RUN mamba create -y -n ros_env python=3.9

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh &&\
    mamba activate ros_env && conda config --env --add channels conda-forge && \
    conda config --env --add channels robostack-staging && \
    conda config --env --remove channels defaults;  \
    mamba install -y ros-noetic-desktop compilers cmake pkg-config make ninja colcon-common-extensions catkin_tools

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC
RUN apt-get update --allow-releaseinfo-change && apt-get install -y --no-install-recommends libgl1 ssh pulseaudio

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    rosdep init && \
    rosdep update

RUN mkdir -p /catkin_ws/src/hts

RUN --mount=type=ssh mkdir -p /hts_git && cd /hts_git && mkdir ~/.ssh && ssh-keyscan github.com >> ~/.ssh/known_hosts && \
    git clone --depth=1 --branch main 'git@github.com:RetroCirce/HTS-Audio-Transformer.git' hts && \
    sed -i 's/from utils/from .utils/g' hts/sed_model.py && \
    sed -i 's/from utils/from ..utils/g' hts/model/htsat.py

RUN --mount=type=ssh cd /tmp && git clone --depth=1 --branch master 'git@github.com:andrewrk/libsoundio.git' && \
    . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /tmp/libsoundio && mkdir build && cd build && cmake .. && make && make install

RUN --mount=type=ssh cd /opt && git clone --depth=1 --branch main 'git@github.com:joextodd/pysoundio.git' && \
    . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd pysoundio && git submodule update --init --recursive && rm -rf pysoundio/libraries/linux/libsoundio* && \
    ln -s /usr/local/lib/libsoundio.a /opt/pysoundio/pysoundio/libraries/linux/libsoundio-2.0.0.a && \
    ln -s /usr/local/lib/libsoundio.so /opt/pysoundio/pysoundio/libraries/linux/libsoundio-2.0.0.so && \
    cd /opt/pysoundio && pip install -e .

COPY ./ /catkin_ws/src/hts

RUN mv /hts_git/hts /catkin_ws/src/hts/src/

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws/src/hts && pip install torchaudio && pip install protobuf==3.20.3 && cd src/hts && pip install -r requirements.txt


RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws && rosdep update && rosdep install --from-paths src --ignore-src -r -y

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws && catkin_make

RUN echo "source /catkin_ws/devel/setup.bash" >> /root/.bashrc
RUN echo "mamba activate ros_env" >> /root/.bashrc
